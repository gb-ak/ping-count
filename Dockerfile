FROM python:3.9.5 AS builder
WORKDIR /build
RUN pip install virtualenv
RUN virtualenv py
COPY ./requirements.txt /tmp/requirements.txt
RUN /build/py/bin/pip install -r /tmp/requirements.txt && rm -f /tmp/requirements.txt

FROM python:3.9.5-slim AS runtime-image
ARG USERNAME=ping
ARG SET_UID=1000
ARG SET_GID=1000
RUN groupadd -g ${SET_GID} -r ${USERNAME} && useradd --no-log-init -u ${SET_UID} -r -g ${USERNAME} ${USERNAME}
WORKDIR /src
RUN chown -R ${USERNAME}:${USERNAME} /src
USER ${USERNAME}
COPY --from=builder /build/py /build/py
ADD ["./main.py", "./db.py", "test_main.py", "/src/"]
EXPOSE 8000
CMD ["/build/py/bin/uvicorn", "main:app", "--host=0.0.0.0"]
