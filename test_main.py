import json
from httpx._transports.mock import MockTransport
from starlette.responses import Response
from uuid import uuid4
from datetime import datetime, timedelta
import os
import time
from random import randint

from fastapi.testclient import TestClient
import jwt
import httpx
import pytest

try:
    from main import app, settings
    import main
except ModuleNotFoundError:
    from .main import app, settings
    from . import main

JWT_KEY = 'secret32secret32secret32secret32'.encode('ascii')

def u() -> str:
    return str(uuid4())

coins_from_pings = lambda ping_cnt: ping_cnt * settings['COINS_PER_PING'] + settings['START_COINS']

def load_mocks():
    with open('./httpmocks/mocks.json') as f:
        data = json.load(f)
    res = {expectation['id']: expectation["httpRequest"] for expectation in data}
    mock_host = os.environ['PING_TWITCH_API_HOST'] + '/mockserver'
    while True:
        try:
            httpx.put(mock_host + '/status')
        except httpx.ConnectError:
            time.sleep(0.5)
        else:
            break

    for expectation in data:
        httpx.put(mock_host + '/expectation', json=expectation)
    return res

http_mocks = load_mocks()

def create_jwt_token(user_id: str = None, exp_date: datetime = None, channel_id: str = 'test_channel1') -> str:
    if not user_id:
        user_id = u()
    if not exp_date:
        exp_date = datetime.now() + timedelta(days=1)
        exp_date = int(exp_date.timestamp())
    data = {'opaque_user_id': user_id, 'exp': exp_date, 'channel_id': channel_id}
    return jwt.encode(data, JWT_KEY, algorithm="HS256")

def test_first_ping_success():
    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    with TestClient(app) as c:
        response = c.post('/ping', headers=headers)
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'OK'
    assert j['cnt'] == 1
    assert j['coins'] == coins_from_pings(j['cnt'])
    assert j['coins_per_ping'] == settings['COINS_PER_PING']
    assert j['ping_interval'] == settings['PINGS_INTERVAL']

def test_second_ping_error():
    save_curr_time = main.curr_time_id
    main.curr_time_id = lambda: 0

    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    with TestClient(app) as c:
        _ = c.post('/ping', headers=headers)
        response = c.post('/ping', headers=headers)
    main.curr_time_id = save_curr_time
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'error'
    assert j['error'] == 'too many requests'

def test_multiple_pings():
    save_curr_time = main.curr_time_id
    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    pings_cnt = 10
    with TestClient(app) as c:
        for i in range(pings_cnt):
            main.curr_time_id = lambda: i
            response = c.post('/ping', headers=headers)
    
    main.curr_time_id = save_curr_time
    assert response.status_code == 200
    j = response.json()
    assert j['cnt'] == pings_cnt
    assert j['coins'] == coins_from_pings(j['cnt'])

def test_delete():
    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    get_info_payload = {'user_id': user, 'access_token': 'test_token'}
    with TestClient(app) as c:
        _ = c.post('/ping', headers=headers)
        _ = c.post('/delete_user', json=get_info_payload)
        response = c.post('/ping', headers=headers)
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'OK'
    assert j['cnt'] == 1
    assert j['coins'] == coins_from_pings(j['cnt'])

def test_expired_jwt_error():
    exp_date = datetime.now() - timedelta(days=1)
    exp_date = int(exp_date.timestamp())
    token = create_jwt_token(exp_date=exp_date)
    headers = {'Authorization': f'Bearer {token}'}
    with TestClient(app) as c:
        response = c.post('/ping', headers=headers)
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'error'
    assert 'JWT token' in j['error']

def test_get_info_success():
    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    get_info_payload = {'user_id': user, 'access_token': 'test_token'}
    with TestClient(app) as c:
        _ = c.post('/ping', headers=headers)
        response = c.post('/user_info', json=get_info_payload)
    
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'OK'
    assert j['cnt'] == 1
    assert j['coins'] == coins_from_pings(j['cnt'])
    assert 'is_registered' in j
    assert j['is_registered'] == False

def test_update_registered():
    save_curr_time = main.curr_time_id
    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    get_info_payload = {'user_id': user, 'access_token': 'test_token'}
    pings_cnt = 10
    with TestClient(app) as c:
        for i in range(pings_cnt):
            main.curr_time_id = lambda: i
            _ = c.post('/ping', headers=headers)
        _ = c.post('/register', json=get_info_payload)
        response = c.post('/ping', headers=headers)
    
    main.curr_time_id = save_curr_time
    assert response.status_code == 200
    j = response.json()
    assert j['is_registered']
    assert j['cnt'] == pings_cnt
    assert j['coins'] == coins_from_pings(j['cnt'])

def test_dont_cnt_pings_after_register():
    save_curr_time = main.curr_time_id
    main.curr_time_id = lambda: 0

    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    get_info_payload = {'user_id': user, 'access_token': 'test_token'}
    with TestClient(app) as c:
        _ = c.post('/ping', headers=headers)
        _ = c.post('/register', json=get_info_payload)
        main.curr_time_id = lambda: 1
        response = c.post('/ping', headers=headers)
    
    main.curr_time_id = save_curr_time
    assert response.status_code == 200
    j = response.json()
    assert j['is_registered']
    assert j['cnt'] == 1
    assert j['coins'] == coins_from_pings(j['cnt'])

def test_get_info_not_found():
    get_info_payload = {'user_id': u(), 'access_token': 'test_token'}
    with TestClient(app) as c:
        response = c.post('/user_info', json=get_info_payload)
    
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'error'
    assert j['error'] == 'not found'

def test_get_info_bad_token():
    get_info_payload = {'user_id': u(), 'access_token': 'bad'}
    with TestClient(app) as c:
        response = c.post('/user_info', json=get_info_payload)
    
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'error'
    assert j['error'] == 'bad token'

def test_get_users():
    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    get_users_params = {'access_token': settings['TOKEN']}
    with TestClient(app) as c:
        _ = c.post('/ping', headers=headers)
        response = c.get('/all_users', params=get_users_params)
    
    assert response.status_code == 200
    j = response.json()
    assert 'users' in j
    users = j['users']
    assert len(users) > 0
    assert user in users

def test_openapi():
    with TestClient(app) as c:
        response = c.get('/openapi.json')
    assert response.status_code == 200

def test_reach_limit():
    global settings
    save_settings = settings.copy()
    settings['COINS_PER_PING'] = 10
    settings['MAX_COINS'] = 15
    save_curr_time = main.curr_time_id
    main.curr_time_id = lambda: 0

    user = u()
    token = create_jwt_token(user)
    headers = {'Authorization': f'Bearer {token}'}
    with TestClient(app) as c:
        _ = c.post('/ping', headers=headers)
        main.curr_time_id = lambda: 1
        response = c.post('/ping', headers=headers)

    settings = save_settings
    main.curr_time_id = save_curr_time
    assert response.status_code == 200
    j = response.json()
    assert j['status'] == 'error'
    assert j['error'] == 'limit reached'
    assert j['coins'] == 15

def clear_mock_server_log(mock_host):
    while True:
        try:
            httpx.put(mock_host + '/clear', params={'type': 'LOG'})
        except httpx.ConnectError:
            time.sleep(0.5)
        else:
            break

@pytest.mark.asyncio
async def test_twitch_pubsub_sent(mocker):
    mock_host = os.environ['PING_TWITCH_API_HOST'] + '/mockserver'
    clear_mock_server_log(mock_host)
    mocker.patch('main.get_pubsub_auth_token', return_value='any')
    add_payload = {'access_token': settings['TOKEN']}
    add_payload['games'] = [{'n': 'Name 1', 'c': randint(1, 100)}, {'n': 'Name 2', 'c': randint(1, 100)}]
    await main.db.connect()
    async with httpx.AsyncClient(app=main.app, base_url="http://test") as ac:
        await ac.post('/games_coins', json=add_payload)
    await main.send_twitch_games.__wrapped__()
    await main.db.close()
    verify_data = {
        "httpRequest": http_mocks['TwitchPubSubOK'],
        "times": { "atLeast": 1, "atMost": 1 }
    }
    verification_resp = httpx.put(mock_host + '/verify', json=verify_data)
    assert verification_resp.status_code == 202

    sent_request_data = httpx.put(mock_host + '/retrieve', json={"httpRequest": http_mocks['TwitchPubSubOK']}).json()[-1]['body']['json']
    assert 'message' in sent_request_data
    del add_payload['access_token']
    assert json.loads(sent_request_data['message']) == add_payload

def test_get_games():
    with TestClient(app) as c:
        response = c.get('/games_coins')

    assert response.status_code == 200
    j = response.json()
    assert 'infos' in j

def test_add_games_info():
    add_payload = {'access_token': settings['TOKEN']}
    add_payload['games'] = [{'n': 'Name 1', 'c': randint(1, 100)}, {'n': 'Name 2', 'c': randint(1, 100)}]
    dt = datetime.now().isoformat()
    with TestClient(app) as c:
        add_resp = c.post('/games_coins', json=add_payload)
        get_resp = c.get('/games_coins')
    
    assert add_resp.status_code == 200
    j = add_resp.json()
    assert j['status'] == 'ok'

    assert get_resp.status_code == 200
    j = get_resp.json()
    assert 'infos' in j
    assert len(j['infos']) >= 1
    last_info = j['infos'][-1]
    assert last_info['info_id'][:18] == dt[:18]
    assert last_info['games'] == add_payload['games']
