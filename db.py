'Simple Redis singleton adapter'
import os

from aioredis import create_redis_pool

__redis = {}

async def connect() -> None:
    'init redis connection'
    redis_address = os.environ.get('PING_REDIS_ADDRESS', 'redis://127.0.0.1')
    if 'connect' in __redis:
        await close()
    __redis['connect'] = await create_redis_pool(redis_address)

async def get_value(key: str) -> str:
    'proxy to redis'
    return await __redis['connect'].get(key)

async def set_value(key: str, value: str) -> None:
    'proxy to redis'
    await __redis['connect'].set(key, value)

async def get_keys(pattern: str) -> list[bytes]:
    'proxy to redis'
    return await __redis['connect'].keys(pattern)

async def del_key(key: str) -> None:
    'proxy to redis'
    await __redis['connect'].delete(key)

async def close() -> None:
    'close and wait for connection'
    __redis['connect'].close()
    await __redis['connect'].wait_closed()
    del __redis['connect']
