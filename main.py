'''
small service that control ping messages count from twitch viewers
'''
from time import time
import json
import os
from typing import Optional
import base64
from datetime import datetime

from fastapi import FastAPI, Header, HTTPException
from pydantic import BaseModel
import jwt
from fastapi_utils.tasks import repeat_every
import httpx

try:
    import db
except ModuleNotFoundError:
    from . import db

read_whitelist = lambda white_list: [s for s in white_list.split(';') if s]

settings = {
    'TOKEN': os.environ.get('PING_ACCESS_TOKEN', 'test_token'),
    'MAX_COINS': int(os.environ.get('PING_MAX_COINS', '500')),
    'START_COINS': int(os.environ.get('PING_START_COINS', '100')),
    'COINS_PER_PING': int(os.environ.get('PING_COINS_PER_PING', '5')),
    'CHANNEL_WHITELIST': read_whitelist(os.environ.get('PING_CHANNEL_WHITELIST', 'test_channel1;test_channel2')),
    'TWITCH_API_KEY': os.environ.get('PING_TWITCH_API_KEY', 'c2VjcmV0MzJzZWNyZXQzMnNlY3JldDMyc2VjcmV0MzI='),
    'PINGS_INTERVAL': int(os.environ.get('PING_PINGS_INTERVAL', 1)),
    'TWITCH_API_HOST': os.environ.get('PING_TWITCH_API_HOST', 'https://api.twitch.tv'),
    'TWITCH_CLIENT_ID': os.environ.get('PING_TWITCH_CLIENT_ID', 'kccmtud89mtvnbvq0z5ubrq73rgyqp'),
    'DEBUG': os.environ.get('PING_DEBUG'),
}
settings['TWITCH_API_KEY'] = base64.b64decode(settings['TWITCH_API_KEY'].encode('ascii'))
DEFAULT_GAMES_INFO = {'games': [{'n': 'Dota 2', 'c': 100}, {'n': 'CS:GO', 'c': 50}, {'n': 'APEX', 'c': 200}]}

app = FastAPI()

class PingModel(BaseModel):
    'Input model from client. Maybe changed to twitch JWT later'
    user_id: str
    channel_id: str

class UserRequest(BaseModel):
    'Input model to get user info'
    user_id: str
    access_token: str

class UserModel(BaseModel):
    'Output model with user information'
    cnt: Optional[int]
    coins: Optional[int]
    status: str
    error: Optional[str]
    is_registered: bool = False
    coins_per_ping: Optional[int] = settings['COINS_PER_PING']
    ping_interval: Optional[int] = settings['PINGS_INTERVAL']

class UsersModel(BaseModel):
    'Output for all users in DB'
    users: list[str]
    error: Optional[str]

class GameInfo(BaseModel):
    'Game info for PubSub message'
    c: int
    n: str

class GamesInfo(BaseModel):
    'PubSub message'
    games: list[GameInfo] = []
    info_id: Optional[str] = ''

class GamesInfoRequest(GamesInfo):
    'Add access_token to request'
    access_token: str

class GamesInfoHistory(BaseModel):
    'History of PubSub messages'
    infos: list[GamesInfo] = []

async def get_user(user_id: str) -> dict:
    'returns user from DB'
    info = await db.get_value('user:' + user_id)
    if info:
        info = json.loads(info)
    return info

curr_time_id = lambda: int(time() / settings['PINGS_INTERVAL'])

async def set_info(user_id: str, time_id: int, cnt: int, coins: int, is_registered: bool = False):
    'Update user info in DB'
    info = json.dumps({'last_ping': time_id, 'cnt': cnt, 'coins': coins, 'is_registered': is_registered})
    await db.set_value('user:' + user_id, info)

async def init_settings():
    'Read settings from DB'
    white_list = await db.get_value('settings:CHANNEL_WHITELIST')
    if white_list:
        settings['CHANNEL_WHITELIST'] = read_whitelist(white_list)

@app.on_event("startup")
async def init_app():
    'Init DB and settings'
    await db.connect()
    await init_settings()
    # call repeated function in the first time (only in production)
    if not settings['DEBUG']:
        await send_twitch_games()

@app.on_event("shutdown")
async def shutdown_app():
    'close DB connection'
    await db.close()

@app.get("/all_users", response_model=UsersModel)
async def get_all_users_ids(access_token: str):
    'Returns all users in DB for now'
    resp = UsersModel(users=[])
    if access_token != settings['TOKEN']:
        resp.error = 'bad token'
        return resp
    keys = await db.get_keys('user:*')
    keys = [k.decode('ascii') for k in keys]
    keys = [k.split(':')[-1] for k in keys]
    resp.users = keys
    return resp

@app.post("/user_info", response_model=UserModel)
async def get_user_info(user_info: UserRequest):
    'Return user information to main server'
    resp = UserModel(status='error')
    if user_info.access_token != settings['TOKEN']:
        resp.error = 'bad token'
        return resp

    user = await get_user(user_info.user_id)
    if not user:
        resp.error = 'not found'
        return resp

    resp.status = 'OK'
    cnt = user['cnt']

    resp.is_registered = 'is_registered' in user and user['is_registered']
    resp.cnt = cnt
    resp.coins = user['coins']
    return resp

@app.post('/delete_user')
async def delete_user(user_info: UserRequest):
    'Delete one user'
    if user_info.access_token != settings['TOKEN']:
        return {'status': 'bad token'}
    await db.del_key('user:' + user_info.user_id)
    return {'status': 'ok'}

@app.post("/register", response_model=UserModel)
async def set_user_register(user_info: UserRequest):
    'Return user information to main server'
    resp = await get_user_info(user_info)
    if resp.status != 'OK':
        return resp

    if resp.is_registered:
        # no change
        return resp

    user = await get_user(user_info.user_id)
    resp.is_registered = True
    await set_info(user_info.user_id, user['last_ping'], resp.cnt, resp.coins, resp.is_registered)
    return resp

async def update_channel_white_list(whitelist: list[str]):
    'Set channel whitelist to new value and store it in DB'
    settings['CHANNEL_WHITELIST'] = whitelist
    whitelist = ";".join(whitelist)
    await db.set_value('settings:CHANNEL_WHITELIST', whitelist)

async def check_channel(channel_id: str) -> bool:
    'Returns true if channel in white list + online'
    if channel_id not in settings['CHANNEL_WHITELIST']:
        return False

    return True

@app.post("/ping", response_model=UserModel)
async def ping(authorization: str = Header(None)):
    'Main call from client side -- add one ping to a state'
    curr_time = curr_time_id()
    resp = UserModel(status='error')
    token = authorization.split(' ')[-1]
    try:
        data = jwt.decode(token, settings['TWITCH_API_KEY'], algorithms=['HS256'])
    except jwt.exceptions.PyJWTError as exception:
        resp.error = f'Unable to decode JWT token {exception}'
        return resp

    user_id = data['opaque_user_id']

    user = await get_user(user_id)
    resp.cnt = 1
    resp.coins = settings['COINS_PER_PING'] + settings['START_COINS']
    if user:
        resp.coins = user['coins']
        resp.cnt = user['cnt']
        resp.is_registered = user['is_registered']
        if resp.is_registered is None:
            resp.is_registered = False
        if user['last_ping'] == curr_time:
            resp.error = 'too many requests'
            return resp
        if not resp.is_registered:
            resp.cnt += 1
            resp.coins = min(resp.coins + settings['COINS_PER_PING'], settings['MAX_COINS'])

    await set_info(user_id, curr_time, resp.cnt, resp.coins, resp.is_registered)
    resp.status = 'OK'
    if resp.coins >= settings['MAX_COINS']:
        resp.status = 'error'
        resp.error = 'limit reached'
    return resp

def get_pubsub_auth_token() -> str:
    'Returns JWT token for Twitch pubsub'
    auth_payload = {
        'exp': int(time() + 60 * 60),
        'user_id': 'all',
        'role': 'external',
        'channel_id': 'all',
        'pubsub_perms': {
            'send': ['global']
        }
    }
    jwt_token = jwt.encode(auth_payload, settings['TWITCH_API_KEY'])
    return jwt_token

def convert_json_to_games_info(info: str, db_key: str = None) -> GamesInfo:
    'Converts data from DB to GamesInfo class instance'
    data = json.loads(info)
    res = GamesInfo(**data)
    if db_key:
        info_id = db_key.split(':', 1)[-1]
        res.info_id = info_id
    return res

async def get_last_games():
    'Get from DB last updated games/coins'
    games_keys = await db.get_keys('games:*')
    if not games_keys:
        return DEFAULT_GAMES_INFO
    last_key = max(games_keys)
    db_games_info = await db.get_value(last_key)
    return json.loads(db_games_info)

@repeat_every(seconds=5)
async def send_twitch_games() -> None:
    'Use twitch PubSub to send games data'
    url = settings['TWITCH_API_HOST'] + '/helix/extensions/pubsub'
    payload = {"target": [ "global" ], "is_global_broadcast": True}
    games = await get_last_games()
    payload['message'] = json.dumps(games)
    jwt_token = get_pubsub_auth_token()
    headers = {'Client-Id': settings['TWITCH_CLIENT_ID']}
    headers['Authorization'] = f'Bearer {jwt_token}'
    async with httpx.AsyncClient() as session:
        resp = await session.post(url, json=payload, headers=headers)
        if resp.status_code != 204:
            print(f'Twitch message - bad responce ({resp.status_code}): {resp.text}')

@app.get('/games_coins', response_model=GamesInfoHistory)
async def games_coins():
    'Returns all history of games information'
    games_keys = await db.get_keys('games:*')
    res = GamesInfoHistory()
    if not games_keys:
        return res
    games_keys = [key.decode('utf-8') for key in games_keys]
    for key in sorted(games_keys):
        db_games_info = await db.get_value(key)
        games_info = convert_json_to_games_info(db_games_info, key)
        res.infos.append(games_info)
    return res

@app.post('/games_coins')
async def set_games_coins(info: GamesInfoRequest):
    'Create new games information and set it latest'
    if info.access_token != settings['TOKEN']:
        raise HTTPException(status_code=403, detail='bad token')
    db_key = f'games:{datetime.now().isoformat()}'
    data = GamesInfo(**info.dict(exclude={'access_token'}))
    await db.set_value(db_key, data.json(exclude={'info_id'}))
    return {'status': 'ok'}
